package CS4076._2023;

import robocode.*;

import static robocode.util.Utils.normalRelativeAngleDegrees;

import java.awt.*;

/* Snippets taken from https://robowiki.net/wiki/CirclingBot, credit to Hapiel for some info on how to do a bot moving in a circle
   and for Robocode in general
   more snippets taken from here: http://mark.random-article.com/weber/java/robocode/lesson3.html

 */
public class _21313482_FortnitePizza extends AdvancedRobot {
    private boolean scan;
    // For judging which point to move to next on the x-axis
    private boolean onCircle;
    private pointStruct judgementX;
    // For judging which point to move to next on the y-axis
    private pointStruct judgementY;
    private pointStruct enemyCenter;
    private pointStruct center;
    private byte scanDirection = 1;
    private boolean enemyScanned;
    private boolean collided;
    private boolean reachedEnemyCircle;
    private String target;
    private double minRadius = 50;
    private double thetaMain;
    private boolean moving;
    private boolean inBoundary;
    // Taken from the RamFire bot
    private int turnDirection = 1;
    private double circumference;
    private ScannedRobotEvent enemy;
    private RobotStatus status;

    public void run() {
        setup();
        setAhead(40000);
        setTurnRadarLeft(360);
        judgementX = new pointStruct(getX() + 5, getY());
        judgementY = new pointStruct(getX(), getY() + 5);
        while (getEnergy() > 0) {
            if(!collided){
                if (getX() <= 10 || getY() <= 10 || getBattleFieldWidth() - getX() <= 10 || getBattleFieldHeight() - getY() <= 10) {
                    inBoundary = true;
                } else {
                    inBoundary = false;
                }
                if(inBoundary) {
                    reverse();
                }
                execute();
                spinRadar();
                rotateGun();
            }
            collided = false;
        }
        enemyScanned = false;
    }

    private void setup() {
        int ran = (int) (Math.random() * 2);
        switch (ran) {
            case 0:
                peppinoSpaghetti();
                break;
            case 1:
                fortnite();
                break;
        }
        setAdjustRadarForRobotTurn(true);
        setAdjustGunForRobotTurn(true);
        setAdjustRadarForGunTurn(true);
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        enemyScanned = true;
        enemy = e;
        target = e.getName();
        scanDirection *= -1;
        // I believe this should be the distance between us and them???
        double absoluteBearing = getHeading() + e.getBearing();
        if (e.isSentryRobot()) {
            return;
        }
        if (e.getBearing() >= 0) {
            this.turnDirection = 1;
        } else {
            this.turnDirection = -1;
        }
        double angleEnemy = e.getBearing();

        double angle = Math.toRadians(status.getHeading() + angleEnemy % 360);

        double eX = getEnemyX(e, angle);
        double eY = getEnemyY(e, angle);
        enemyCenter = new pointStruct(eX, eY);
        center = new pointStruct(getX(),getY());
        //moveCircle(eX, eY, e.getDistance());
        if(pointStruct.isOnCircle(center,enemyCenter,minRadius)) {
                // pointStruct.getDistance(enemyCenter,center)
                moveCircle(10,36,20);

        } else {
            setAhead(400);
        }
    }

    // The idea is to use Bresenham's circle drawing algorithm to spin around a singular or small group of enemies
    // That worked out badly
    /*
    NEW PLAN:
    generate a small circle around the enemy of center eX, eY of radius r
    if we are on any point of the circle we move in said circle
    POINT OF REFERENCE : https://math.stackexchange.com/questions/260096/find-the-coordinates-of-a-point-on-a-circle
     */
    private void moveCircle(double radius, int maxPoints, int pixels) {
        double theta = (double) 90 / maxPoints;
        // Each for loop should map to a quadrant
        // This should be one quarter, now to figure out the other 4
        // This is across x, down y
        int midpoint = maxPoints / 2 ;
        for (int i = 0; i < maxPoints; i++) {
                // Gradually turn in an arc of 15 degrees because 90 degrees has been split up into 6 maxPoints
                setTurnRight(theta);
                // We have to move across the arc that we have generated from turning
                setAhead(theta * radius + pixels);
                // If we are at the half way point, FIRE
                if(i >= midpoint) {
                    spinRadar();
                }
                rotateGun();
                setFire(1);
        }
        execute();
    }

    public void onHitRobot(HitRobotEvent e) {
        if (e.isMyFault()) {
            collided = true;
            reverse();
        }
    }
    // http://mark.random-article.com/weber/java/robocode/lesson4.html code for turning the gun
    private void rotateGun(){
            setTurnGunRight(getHeading() - getGunHeading() + enemy.getBearing());
    }


    public void onHitWall(HitWallEvent e) {
        collided = true;
        reverse();
    }

    private void reverse() {
        setBack(4000);
        setTurnRight(180);
    }

    private void spinRadar() {
        int max = 0;
        int degrees = 30;
        scan = true;
        if(enemyScanned) {
            //setTurnRadarRight(getHeading() - getGunHeading() + enemy.getBearing());
            setTurnRadarRight(180 * scanDirection);
            setTurnGunRight(90);
            return;
        }
        execute();
        while (scan) {
            turnRadarRight(degrees);
            max += degrees;

            if (max > 360) {
                scan = false;
                break;
            }
        }
    }

    // laser in the gun's focus towards the enemy
    private void focusGun() {
        double absoluteBearing = getHeading() + enemy.getBearing();
        double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading());
        double bearingFromRadar = normalRelativeAngleDegrees(absoluteBearing - getRadarHeading());
    }
    // Taken from https://stackoverflow.com/questions/22107351/robocode-how-to-get-the-enemies-co-ordinates

    public void onStatus(StatusEvent e) {
        this.status = e.getStatus();
    }

    private double getEnemyX(ScannedRobotEvent e, double angle) {
        return (status.getX() + Math.sin(angle * e.getDistance()));
    }

    private double getEnemyY(ScannedRobotEvent e, double angle) {
        return (status.getY() + Math.cos(angle * e.getDistance()));
    }

    //YYYYYYYYYYYYYYYYYEEEEEEEEEEOOOOOOOOOOOOOOOOOWWWWWWWW
    private void peppinoSpaghetti() {
        setBodyColor(Color.black);
        setRadarColor(Color.white);
        setBulletColor(Color.white);
        setScanColor(Color.white);

    }

    private void fortnite() {
        setBodyColor(Color.BLUE);
        setRadarColor(Color.ORANGE);
        setBulletColor(Color.white);
        setScanColor(Color.ORANGE);
    }
}

//Pseudo struct
class pointStruct {
    private double x;
    private double y;

    pointStruct() {
    }

    pointStruct(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public static double getDistance(pointStruct p1, pointStruct p2){
        double subX = Math.pow(p2.x -p1.x, 2);
        double subY = Math.pow(p2.y - p1.y, 2);
        return Math.sqrt(subX + subY);
    }

    public static boolean isOnCircle(pointStruct p1, pointStruct p2, double radius) {
        return pointStruct.getDistance(p2, p1) > Math.pow(radius, 2) - 10 ||
                pointStruct.getDistance(p2, p1) < Math.pow(radius, 2) + 10;
    }
}
