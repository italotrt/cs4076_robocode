package CS4076._2023;
import robocode.*;
import java.lang.Math.*;
import java.awt.Color;

 

public class _21335265_BrendanSwords extends AdvancedRobot
{
int forwardOrBack = 1;	
boolean forwardVelocity = false;
Color bodyColor = new Color(0.38f, 1.0f, 0.31f);
Color gunColor =  new Color(0.97f,0.48f,1f);
Color radarColor = new Color(0f, 1f, 0.945f);
int noOfRobots = 0;
public void run(){
setColors(bodyColor, gunColor, radarColor);
		noOfRobots = getOthers();
		boolean start = true;
				
		while(start){	
		if(noOfRobots>16){
		setMaxVelocity(5);
		setTurnRight(10000);
		setAhead(10000);
		execute();
		
		}else{
		setAdjustRadarForRobotTurn(true);
		turnRadarRight(360000);

	}
			
		}
}
		
	public void onScannedRobot(ScannedRobotEvent e) {

	if(noOfRobots > 16){

			setFire(3);
			execute();
			return;
}
	
if(e.isSentryRobot()){
	double battlefieldX = getBattleFieldWidth() / 2;
	double battlefieldY = getBattleFieldHeight() / 2;
    
		double normalizedAim = fixAim(Math.atan2(battlefieldX - getX(), battlefieldY - getY()) - getHeadingRadians() ) ;
	
    setTurnRightRadians(fixAim(normalizedAim - getHeadingRadians()));
	
    setAhead(100);

	return;
}

	if((Math.random() * 1000) > 998){
		setTurnRight(90);
}


	if(e.getDistance() >= 150){
	
		double turnDistance = fixAim( e.getBearing() + getHeading() - getGunHeading() );
		if(e.getVelocity() < 5){
	
			setTurnGunRight(turnDistance);
		}else{
			setTurnGunRight(turnDistance + 20);
			}
	
		setAhead((e.getDistance() - 100) * forwardOrBack);
		setFire(2);
					System.out.println(e.getBearing());	
		System.out.println("2!");
	}else{
		double turnDistance = fixAim( e.getBearing() + getHeading() - getGunHeading() );
			
			setTurnGunRight(turnDistance);
		
		setFire(3);


}
		
	}

	
public void onHitRobotEvent(HitRobotEvent e){
	if(e.isMyFault()){
	setBack(50);
	}
}
	
	
public	double fixAim(double angle){
	if(angle > 180){return angle-360;}
	if(angle < -180){return angle+360;}
	return angle;
}
	 
public void onHitWall(HitWallEvent e) {
	if(noOfRobots > 16){	
		if(forwardVelocity){
			setBack(1000);
			forwardVelocity = false;
		}else{
			setAhead(1000);
			forwardVelocity = true;
		}
}else{
	forwardOrBack *= -1;
}
}

	

}
