package CS4076._2023;
import robocode.*;
import java.awt.geom.*;
import robocode.util.Utils;
import java.awt.*;

/**
 * Class: ElRoboto
 *
 * ElRoboto is an AdvancedRobot that extends Robocode's AdvancedRobot class. 
 * It implements custom behaviors, scans for enemies, moves within sentry borders, 
 * and aims and shoots at predicted enemy positions. 
 */
public class _21344256_ElRoboto extends AdvancedRobot {
    
    static double xMomentum;
    static double yMomentum;
    static double priorDist;
    
    public void run() {
	    setBodyColor(Color.white);
        setRadarColor(Color.green);
		setScanColor(Color.red);
        setAdjustGunForRobotTurn(true);
        turnRadarRightRadians(Double.POSITIVE_INFINITY);
    }
    
private void stayAwayFromWalls() {
    double x = getX();
    double y = getY();
    double heading = getHeadingRadians();
    double battleFieldWidth = getBattleFieldWidth();
    double battleFieldHeight = getBattleFieldHeight();
    double margin = getSentryBorderSize(); // how far away from the walls the robot should stay

    if (x < margin || x > battleFieldWidth - margin || y < margin || y > battleFieldHeight - margin) {
        // If the robot is too close to a wall, turn away from it
        double turnAngle = Utils.normalRelativeAngle(Math.atan2(battleFieldHeight - y, battleFieldWidth - x) - heading);
        setTurnRightRadians(turnAngle);
        setAhead(Double.POSITIVE_INFINITY); // move away from the wall
    }
}

    public void onScannedRobot(ScannedRobotEvent detected) {
	stayAwayFromWalls();
if (detected.isSentryRobot()) {
            return;
        }
    double absB = detected.getBearingRadians() + getHeadingRadians();
    double d = detected.getDistance();
        
	double alpha = 0.1; // Smoothing factor, where 0 < alpha <= 1. Higher values give more weight to recent data.
	//used as simple ruuning average - https://robowiki.net/wiki/Rolling_Averages 

	xMomentum = (1 - alpha) * xMomentum + alpha * (-Math.sin(absB) / d);
	yMomentum = (1 - alpha) * yMomentum + alpha * (-Math.cos(absB) / d);

        
    double xPosition = getX();
double yPosition = getY();
double battleFieldWidth = getBattleFieldWidth();
double battleFieldHeight = getBattleFieldHeight();

// Calculate the attractive forces from the walls
double xForceFromWalls = 1 / xPosition - 1 / (battleFieldWidth - xPosition);
double yForceFromWalls = 1 / yPosition - 1 / (battleFieldHeight - yPosition);

// Combine the attractive forces with the current xMomentum and yMomentum
double xCombinedForce = xMomentum + xForceFromWalls;
double yCombinedForce = yMomentum + yForceFromWalls;

// Calculate the desired heading based on the combined forces
double desiredHeading = Math.atan2(xCombinedForce, yCombinedForce);

// Calculate the angle difference between the current heading and the desired heading
double angleDifference = Utils.normalRelativeAngle(desiredHeading - getHeadingRadians());

// Set the turn direction
setTurnRightRadians(angleDifference);
        
setAhead(120 - Math.abs(getTurnRemaining()));
        
//nabbed from https://robowiki.net/wiki/Talk:Selecting_Fire_Power
double maxBulletPower = 2.4999;
double energyDivisor = 7;

// Calculate the bullet power based on the robot's energy
double bulletPower = Math.min(maxBulletPower, getEnergy() / energyDivisor);

// Fire the bullet and check if it was successfully fired
Bullet firedBullet = setFireBullet(bulletPower);
if (firedBullet != null) {
    priorDist = Double.POSITIVE_INFINITY;
}

        if (priorDist + 100 > d) {
            priorDist = d;

            if (getGunHeat() < 1) {
                setTurnRadarLeft(getRadarTurnRemaining());
            }
            
//aiming taken from Infinity system
            setTurnGunRightRadians(Utils.normalRelativeAngle(absB - getGunHeadingRadians() +
                (detected.getVelocity() /  11) * Math.sin(detected.getHeadingRadians() - absB)));
        }
    }
}



