package CS4076._2023;
import robocode.*;
import java.awt.Color;

/**
 * Robotnik - a robot by Jacob
 */
public class _21304149_DoctorRobotnik extends Robot
{
	boolean passive = false;
	boolean superKillMode = false;
	int turnDirection = 1;
	public void run() {
		
		setBodyColor(Color.cyan);
		setGunColor(Color.blue);
		setRadarColor(Color.cyan);
		setBulletColor(Color.green);
		setScanColor(Color.yellow);

		// Robot main loop
		while(true) {
			borderCheck();
			if(!superKillMode){
				turnGunRight(360);
		    	double distance = Math.random()*250;//Random movement
           	double angle = Math.random()*45; //Random turn
		   		ahead(distance);
				turnRight(angle);
				if(getOthers() <= 1 && getEnergy() > 20){
					superKillMode = true; //One left- Go for a ram kill
				}
			}else{
				if(getEnergy() < 20){ //If on low health, duck out of aggressive mode
					superKillMode = false;
				}else{
					turnRight(5 * turnDirection);
				}
			}
			
		}
	}

	void borderCheck(){
		if(getX() <= 200){
				scamper();
		}else if(getX() >= 200){
			scamper();
		}else if(getY() <= 200){
			scamper();
		}else if(getY() >=  00){
			scamper();
		}
	}

	void scamper() {
    	turnRight(180);
		ahead(Math.random()*125);
	}

	public void onBulletHitBullet(BulletHitBulletEvent e){
		borderCheck();
		if(!superKillMode){
			fire(2); // Go for another shot
		}
	}

	public void onHitRobot(HitRobotEvent e) {
       if(!superKillMode){ //Neutral mode - Run away!!
			if(e.getBearing() > -90 && e.getBearing() <= 90){
           	back(100);
       		}else{
           	ahead(100);
       		}
		}else{ //Aggressive mode
			if(e.getBearing() >= 0){
				turnDirection = 1;
			}else{
				turnDirection = -1;
			}
			turnRight(e.getBearing());
			
			//Go for a ram kill
			if(e.getEnergy() > 16){
				fire(3);
			}else if(e.getEnergy() > 10){
				fire(2);
			}else if(e.getEnergy() > 4){
				fire(1);
			}else if (e.getEnergy() > 2){
				fire(.5);
			}else if(e.getEnergy() > .4){
				fire(.1);
			}
			ahead(40);
		}	
   }

	public void onScannedRobot(ScannedRobotEvent e){
		if(!e.isSentryRobot()){ //Don't want to attack a sentry!
			if(!superKillMode){ //Non aggressive mode - Judges the distance before firing
				if(!passive){
    				fire(3);
					double distance = e.getDistance();
					if(distance<200){
       					fire(3.5);
       				}else if(distance<500){
           			fire(2.5);
       				}else if(distance<800){
           			fire(1.5);
       				}else{
           			fire(0.5);
       				}
	   			}else{
					fire(0.5);
				}
				turnRight(90);
    			ahead(150);
				if(getEnergy() < 20){ //Checking if he should go into passive mode
					passive = true;
				}else{
					passive = false;
				}
			}else{ //Aggressive mode - Run him over!!
				if(e.getBearing() >= 0){
					turnDirection = 1;
				}else{
					turnDirection = -1;
				}
				turnRight(e.getBearing());
				ahead(e.getDistance() + 5);
				scan();
			}
		}
	}

	//On being shot :(
	public void onHitByBullet(HitByBulletEvent e) {
		if(!superKillMode){
			maneuver();
		}else{
			if(getEnergy() < 30){ //If on low health, Robotnik will chicken out of aggressive mode
				superKillMode = false;
				maneuver();
			}
		}
	}
	
	public void maneuver(){
		double angle = Math.random()*180;
		turnLeft(angle);
		ahead(300); //He'll try to run away after a random turn
	}

	//If he hits a wall, he'll back up
	public void onHitWall(HitWallEvent e) {
		back(50);
		turnRight(150);
	}	
}