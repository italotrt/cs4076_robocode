package CS4076._2023;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.TurnCompleteCondition;


public class CannonFodder extends AdvancedRobot {
	
	
    public void run() {
    	setBodyColor(Color.BLUE);
    	setGunColor(Color.BLUE);
    	setRadarColor(Color.BLUE);
        while (true) {
        	setAhead(50000);
			setTurnRight(90);
			waitFor(new TurnCompleteCondition(this));
			setTurnLeft(90);
			waitFor(new TurnCompleteCondition(this));
			setTurnRight(180);
			waitFor(new TurnCompleteCondition(this));
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        if (e.isSentryRobot()) {
        	run();
        }
        fire(3);
        
    }
    
    public void onHitByBullet(HitByBulletEvent e) {
    	setBodyColor(Color.RED);
    	setGunColor(Color.RED);
		setRadarColor(Color.RED);
    }
    
    public void onHitWall(HitWallEvent e) {
    	turnRight(180);
    	ahead(500);
    }
    
}