package ac;
import robocode.*;
//import java.awt.Color;

/**
 * YupMossySham - A robot by Adam Collins (21332967)
 */
import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;

import java.awt.*;

public class _21332967_YupMossySham extends AdvancedRobot {

	// circle go brrr
    public void run() {
        // i like black :)
        setBodyColor(Color.black);
        setGunColor(Color.black);
        setRadarColor(Color.black);
        setScanColor(Color.black);

        // cheeky loop to keep spinning (forever :P)
        while (true) {
            setTurnRight(10000); // he do be spinning doe :O
            setMaxVelocity(5);
            ahead(10000);
        }
    }

	// see a robot? we shooting! 
	// its on sight fr >:)
    public void onScannedRobot(ScannedRobotEvent e) {
        fire(3);
    }

	// if we hit a mf then we gonna stop turning nd moving,
	// gonna have to turn again to keep the spin alive ;)
	// complicated, i know, but its KEY to our success.
    public void onHitRobot(HitRobotEvent e) {
        if (e.getBearing() > -10 && e.getBearing() < 10) {
            fire(3);
        }
        if (e.isMyFault()) {
            turnRight(10);
        }
    }
	
	// fortnite victory royale emote >:D
	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
}