package CS4076._2023;

import robocode.*;

public class _21332142_Still_No_CS2 extends AdvancedRobot {

    public void run() {
        while (true) {
            setTurnRight(10000);
            setMaxVelocity(5);
            ahead(10000);
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {

        if(!(getEnergy() < 1)) {
            fire(3*(getEnergy()/100));
        }
    }

}
