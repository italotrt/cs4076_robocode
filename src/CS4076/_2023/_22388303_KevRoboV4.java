package CS4076._2023;
import robocode.*;

import java.util.Random;
import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.BulletHitEvent;
import robocode.Event;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.Rules;
import robocode.ScannedRobotEvent;
import robocode.BulletMissedEvent;
import robocode.util.Utils;
import robocode.BulletHitEvent;
/*

To anyone peeking my code,
I have spent a signifigant portion of my life on this.More than I would like to admit.To put it simply KevRobo Fucks he fucks hard.
FEAR ME


*/
public class _22388303_KevRoboV4 extends AdvancedRobot {

  int moveCount, moveDirection = 1, moveMode = 1, radarDirection = 1, count = 0 , wallMargin = 100,avoidWallHandler = 0;
  Opponent target = new Opponent();
  double turnAmt, gunTurnAmt,radarTurnAmt;

  public void run() {
    //sepeate gun,radar and movement
    setAdjustRadarForGunTurn(true);
    setAdjustGunForRobotTurn(true);

    //reset target
    target.reset();
    //startradar
    setTurnRadarRight(360);

    while (true) {
    //check if too close to wall
      if (getX() <= wallMargin || getY() <= wallMargin || getBattleFieldWidth() - getX() <= wallMargin || getBattleFieldHeight() - getY() <= wallMargin) {
        if (avoidWallHandler <= 0) {
            //forces robot to handle evenet instead of getting stuck
          avoidWallHandler += wallMargin;
          setMaxVelocity(0); 
        }
      }

      radarScan();
      movementStrategy();
      Fire();

      execute();

    }
  }

  void Fire() {
    //find bearings
    initBearing();
    if (Math.abs(gunTurnAmt) <= 4) {
      //get the distance between the guns heading and the targets location
      setTurnGunRight(getHeading() - getGunHeading() + target.getBearing());
      //turn Radar to target
      setTurnRadarRight(radarTurnAmt);
      //turn to target and correct a bit
      setTurnRight(containAngle(target.getBearing() + 80));

      //wait till gun  is reset and Energy is ready
      if (getGunHeat() == 0 && getEnergy() > .2) {
      
      //fire at max power or target distance
        setFire(Math.min(400 / target.getDistance(), 3));
      }
    }

    // otherwise just set the gun to turn.
    else {
      setTurnGunRight(gunTurnAmt);
      setTurnRadarRight(radarTurnAmt);
    }
  }

  void radarScan() {
    //find me a target
    if (target.hasTarget()) {
      setTurnRadarRight(360);
    }
    //focus target
    else {
      //creeates a 30 degree cone in the direction of the enemies
      double radarCone = getHeading() - getRadarHeading() + target.getBearing();
      //cone
      radarCone += 30 * radarDirection;
      //set radar to target
      setTurnRadarRight(containAngle(radarCone));
      radarDirection *= -1;
    }

  }

  public void movementStrategy() {
 
   //handles wallmovement
    if (avoidWallHandler > 0) avoidWallHandler--;
   
	 //square off agains target turned slighty so circle 
      setTurnRight(containAngle(target.getBearing() + 90 - (20 * moveDirection)));
	  //movement strat circle
    if (moveMode == 1) {
     
      //change dir on stop
     if (getVelocity() == 0) {
			setMaxVelocity(8);
			moveDirection *= -1;
			setAhead(5000* moveDirection);
		}
    
    } else {
    
      //change dir every .5 sec or on stop
     if (getTime() % 15 == 0 || getVelocity() == 0) {
        setMaxVelocity(8);
        moveDirection *= -1;
        setAhead(50 * moveDirection);
		if(moveCount==4){
		  moveCount=0;
		  moveMode=1;
		}
         moveCount++;
		 
      }
    }
  }

  public void onBulletMissed(BulletMissedEvent e) {
    //reset target if keep missing
    count++;
    if (count == 3) {
      target.reset();
      count = 0;
    }
  }
  //reset count on hit
  public void onBulletHit(BulletHitEvent e) {
    count = 0;
  }
  //change dir if you hit a wall


  public void onScannedRobot(ScannedRobotEvent e) {
    //if a sentry get dir and dip
    if (e.isSentryRobot() == true) {
      setTurnRight(e.getBearing());
	  target.reset();
      while (e.getDistance() <= 30) {
       setBack(20);
      }
    }
    //update target if none or closer than other target or is same target as tracked
    if (target.hasTarget() || e.getDistance() < target.getDistance() - 20 || target.getName().equals(e.getName())||e.isSentryRobot()==false) {
      target.update(e);
    }

  }
  //make agressor new target and evafe
  public void onHitByBullet(HitByBulletEvent e) { 
	 moveMode*=-1;
	 
  }


  //reset target if its the one we are tracking
  public void onRobotDeath(RobotDeathEvent e) {
    if (e.getName().equals(target.getName())) {
      target.reset();
    }
  }

  public void initBearing() {

    //diffrence between  where the body is pointing and distance from the targets tank
    //amount body needs to turn to face target
    turnAmt = getHeading() + target.getBearing();
    //diffrence between the turn amount - where the gun is pointing
    //amount gun needs to turn to face target
    gunTurnAmt = containAngle(turnAmt - getGunHeading());
    //diffrence between th turn amount - where the rader is pointing
    //amount radar needs to turn to face target
    radarTurnAmt = containAngle(turnAmt - getRadarHeading());

  }

  //contains in angle within +- 180 degrees
  double containAngle(double inputAngle) {
    //positive side
    while (inputAngle > 180) inputAngle -= 360;
    //negtive side
    while (inputAngle < -180) inputAngle += 360;
    return inputAngle;
  }

}


//target class nothing fancing
class Opponent {
  private double bearing, distance, heading;
  private String name = "";

//getters
  public double getDistance() {
    return distance;
  }

  public String getName() {
    return name;
  }

  public double getBearing() {
    return bearing;
  }

  public void setBearing(double bearing) {
    this.bearing = bearing;
  }

  public double getHeading() {
    return heading;
  }
//clears target
  public void reset() {
    bearing = 0.0;
    distance = 0.0;
    heading = 0.0;
    name = "";

  }
//returns false if empty
  public boolean hasTarget() {

    if (name == "") {
      return true;
    }
    return false;
  }
  
//add info
  public void update(ScannedRobotEvent e) {
    bearing = e.getBearing();
    distance = e.getDistance();
    heading = e.getHeading();
    name = e.getName();

  }




}
